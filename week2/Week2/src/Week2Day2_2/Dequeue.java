package Week2Day2_2;

import java.util.ArrayDeque;
import java.util.Deque;

public class Dequeue  {

	 

	public static void main(String[] args) {

	 

	// Create deque object using ArrayDeque Class

	Deque<Integer> deque = new ArrayDeque<Integer>();

	 

	// add elements to the deque
	
       deque.add(20);
       deque.add(40);
       deque.add(50);
       deque.add(90);
	// insert an element at the head
       deque.addFirst(100);
	// insert an element at the tail
        deque.addLast(200);
	// print the deque
       System.out.println(deque);
	//peek the first element
       System.out.println(deque.peek());
	//delete all the elements of deque
       System.out.println(deque.removeAll(deque));

	}

	 
}
