package Week2Day1_3;

import java.util.Iterator;
import java.util.Stack;

public class StackImplementation
{
	 public static void main(String args[]) 

	    { 

	        // Create an object of Stack of type String

	       Stack sl=new Stack();
	        // add elements into the Stack 

	        sl.push(30);
	        sl.push(90);
	        sl.push(80);
	        sl.push(70);

	        // Display the Stack 
             System.out.println(sl);
	        

	        // Create an iterator 

	        Iterator ii=sl.iterator();
	     // Display the values using the iterator 

	        while(ii.hasNext())
	        {
	        	Object o=ii.next();
	        	System.out.println(o);
	        }

	        
	        

	      }

	}

