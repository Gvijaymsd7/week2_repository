package Week2Day2_1;

import java.util.PriorityQueue;


public class QueueImplementation 
{

		 

		public static void main(String[] args) {

		//create priorityQueue Object
			PriorityQueue pq=new PriorityQueue();

		//perform addition of elements to priorityQueue 
           pq.add(50);
           pq.add(40);
           pq.add(20);
           pq.add(30);
           System.out.println(pq);
		//print the top element of the priority queue
           pq.peek();
           System.out.println(pq.peek());
		//check if the priority queue contains a particular element
           System.out.println(pq.contains(30));
		//print the size of the queue
            System.out.println(pq.size());
		//print the head of the queue
            System.out.println(pq.peek());
		//delete an element of the queue
          System.out.println(pq.remove(50));
         
		//print the queue elements
           System.out.println(pq);
		//remove all the elements of the queue at once
          System.out.println(pq.removeAll(pq));
		//check if the queue is empty or not
          System.out.println(pq.isEmpty());

		}

		 
}
